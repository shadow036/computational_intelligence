# Final project
## Introduction
For the final project I tried to implement the Monte Carlo tree search procedure from scratch, in order to try out a technique that specifically targets the "games" domain and also take the opportunity to deepen my knowledge of a topic only briefly touched during the lectures.
## References
The references that I used were mainly two:
- the [wikipedia page](https://en.wikipedia.org/wiki/Monte_Carlo_tree_search) of the Monte Carlo tree search
- a [videolecture](https://www.youtube.com/watch?v=UXW2yZndl7U) by Jonh Levine, teaching professor at the University of Strathclyde

I had no other collaborators.
## Used libraries
Apart from standard numerical and libraries and libraries containing some very basic helper functions, all the rest of the code is done exclusively from scratch.
## Results
Unfortunately the results are not good at all and I may have missed some bugs in the code (this or the Monte Carlo tree search is not suitable for the "Quarto" game).

The winrate against the RandomPlayer is quite random and it looks like the MCTS acts as a RandomPlayer itself (the average winrate is something like 50%). 
## Implementation
### Introduction
Concerning the actual implementation, the main differences are located in the new file I created for the occasion, namely "node_class.py" which defines the Node class itself and a subclassed version of the main "Quarto" class.
#### "main.py"
The "main.py" file was only slightly modified by adding the interface for the new player, the "MonteCarloPlayer" and by allowing the possibility to have a sequence of matches in such a way that is possible to analyze the performance on a bigger picture.
#### "object.py"
The "object.py" was left untouched as required: all additional features were implemented via subclassing.
#### "init.py"
The "init.py" simply add the additional file dependency.
#### "node_class.py"
In the "node_class.py" there are two main components :
- The "Node" class which represents a node of the tree and each instance of it has all the necessary methods and attributes to run the MCTS starting from its relative position on the tree.
Each node has the following attributes:
  - "state": contains either a "Quarto" instance or a "CustomQuarto" instance
  - "action type": the action that allows the transition from this node to the children (0 -> choose piece, 1 -> place piece) and the parameter of the previous action that led to this node (piece id if the previous action was a choose, (x, y) position of the board if the previous action was a place)
  - "depth level": depth of the node in the tree, used to limit the visited nodes
  - "cumulative results, total simulations and upper confidence bound": values used in the MCTS method itself
  - "parent and child nodes"
  - "active player": the player that needs to take the specified action

     The methods follow the phases of the MCTS, so there is a selection method, and expansion method, a simulation one (base on Quarto.run and the final backpropagation.
    In addition to that there are also various getters, a method to calculate the upper confidence bound, one for checking if we can output the best action and the one to found the best action itself.
- The "CustomQuarto" class is a subclass of the "Quarto" one and it allows more precise operations on the nodes (namely the possibility to skip the first "choose piece" phase in case the simulation starts from a "place phase".

## Conclusion
Since the results were bad I tried to allow some degree of freedom in the MCTS procedure, by modifying the number of simulations for a single node and by visiting deeper nodes before returning the current best action.

In the end also this additions didnt prove themselves useful.