# Free for personal or classroom use; see 'LICENSE.md' for details.
# https://github.com/squillero/computational-intelligence

from itertools import combinations

from functools import reduce

import logging
import argparse
import random
import quarto
from copy import deepcopy

UNKNOWN = -2
TOURNAMENT_SIZE = 3


class RandomPlayer(quarto.Player):  # random strategy
    """Random player"""
    def __init__(self, quarto: quarto.Quarto) -> None:
        super().__init__(quarto)    # calls parent constructor

    def choose_piece(self) -> int:
        return random.randint(0, 15)    # choose phase

    def place_piece(self) -> tuple[int, int]:   # place phase
        return random.randint(0, 3), random.randint(0, 3)


class MonteCarloPlayer(quarto.Player):
    def __init__(self, quarto_c: quarto.Quarto) -> None:
        super().__init__(quarto_c)    # calls parent constructor

    def choose_piece(self) -> int:
        root = quarto.Node(deepcopy(self._Player__quarto), (quarto.CHOOSE_PIECE, UNKNOWN))
        return root.apply_monte_carlo_method()

    def place_piece(self) -> tuple[int, int]:   # place phase
        root = quarto.Node(deepcopy(self._Player__quarto), (quarto.PLACE_PIECE, UNKNOWN))
        return root.apply_monte_carlo_method()


def main():
    wins = [0, 0]
    for i in range(TOURNAMENT_SIZE):
        game = quarto.Quarto()  # get wrapper
        game.set_players((MonteCarloPlayer(game), RandomPlayer(game)))
        winner = game.run()  # run game
        logging.warning(f"main: Winner: player {winner}")
        wins[winner] = wins[winner] + 1
    print(wins)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='count',
                        default=0, help='increase log verbosity')
    parser.add_argument('-d',
                        '--debug',
                        action='store_const',
                        dest='verbose',
                        const=2,
                        help='log debug messages (same as -vv)')
    args = parser.parse_args()

    if args.verbose == 0:
        logging.getLogger().setLevel(level=logging.WARNING)
    elif args.verbose == 1:
        logging.getLogger().setLevel(level=logging.INFO)
    elif args.verbose == 2:
        logging.getLogger().setLevel(level=logging.DEBUG)

    main()  # entry point
