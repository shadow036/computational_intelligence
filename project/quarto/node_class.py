from math import log
from main import UNKNOWN
from itertools import product
from copy import deepcopy

import main
import quarto

CHOOSE_PIECE = 0
PLACE_PIECE = 1

MAX_DEPTH = 2
AMOUNT_SIMULATIONS = 5


class Node:
    def __init__(self, state: quarto.Quarto, action_type: tuple,
                 parent_node=None, depth_level=0, active_player=0) -> None:
        self.state = state
        self.action_type = action_type
        self.depth_level = depth_level
        self.cumulative_results = 0
        self.total_simulations = 0
        self.parent_node = parent_node
        self.child_nodes = []
        self.active_player = active_player
        self.upper_confidence_bound1 = 0

    # MONTECARLO PHASES WRAPPER
    def apply_monte_carlo_method(self):
        while True:
            selected_node = self.selection(None, 0)[0]
            selected_node.expansion()
            if selected_node.check_stopping_condition() or len(selected_node.get_child_nodes()) == 0:
                return self.choose_best_action()
            for extended_node in selected_node.get_child_nodes():
                for _ in range(AMOUNT_SIMULATIONS):
                    score = extended_node.simulation()
                    extended_node.backpropagation(score)
            self.compute_upper_confidence_bound1()
            for mc in self.get_child_nodes():
                mc.compute_upper_confidence_bound1()

    # MONTECARLO PHASES

    def selection(self, selected_node, max_):
        max__ = max_
        if len(self.get_child_nodes()) == 0 and self.get_upper_confidence_bound() >= max__:
            max__ = self.get_upper_confidence_bound()
            selected_node = self
        elif len(self.get_child_nodes()) > 0:
            for c in self.get_child_nodes():
                selected_node, max__ = c.selection(selected_node, max__)
        return selected_node, max__

    def expansion(self):
        choose_actions = None
        place_actions = None
        if self.get_action_type()[0] == CHOOSE_PIECE:
            # choose only available pieces
            state = self.get_state().get_board_status()
            state = list(state.flatten())
            choose_actions = [x for x in range(16) if x not in state]
        else:
            temp = 2 * [list(range(4))]
            # place only on free tiles
            place_actions = [(x, y) for (x, y) in list(product(*temp)) if self.get_state().get_board_status()[x, y] < 0]

        if self.get_action_type()[0] == CHOOSE_PIECE:
            for a in choose_actions:
                # create a Quarto-like class instance for the child node
                cq = CustomQuarto()
                # the opponent of the Montecarlo tree search will be the random player
                opponent = main.RandomPlayer(cq)
                # but also the stochastic part of the Montecarlo tree search uses a random search;
                # we will distinguish between the two in the following
                cq.set_players((main.RandomPlayer(cq), opponent))
                # create the edge between the current node and the future child
                # new_edge = Edge(self, CHOOSE_PIECE, a)
                # define the chosen piece, in such a way that it will be used in the following phase
                cq.set_selected_piece_index(a)
                # copy the board of the parent node, since in the choose phase it doesn't change
                cq.set_board(deepcopy(self.get_state().get_board_status()))
                # generate the child node and link it with the corresponding edge;
                # set the flag specifying the action that will be done by the new node (e.g. PLACE)
                child_node = Node(cq, (PLACE_PIECE, a), parent_node=self, depth_level=self.get_depth_level() + 1,
                                  active_player=(self.active_player + 1) % 2)
                # same linking as above but in the opposite direction
                # new_edge.set_child_node(child_node)
                # update the children list of the current node
                self.get_child_nodes().append(child_node)
        else:
            for a in place_actions:
                cq = CustomQuarto()
                # the selected piece was chosen previously: the child must use this information
                if self.get_action_type()[1] != UNKNOWN:
                    cq.set_selected_piece_index(self.get_action_type()[1])
                # temporarily set the child board as the same as the current node one
                cq.set_board(self.get_state().get_board_status())
                # update the tile in the chosen position,
                # automatically filling it with the piece specified before
                cq.set_tile(a[0], a[1])
                child_node = Node(cq, (CHOOSE_PIECE, a), parent_node=self, depth_level=self.get_depth_level() + 1,
                                  active_player=self.active_player)
                # new_edge.set_child_node(child_node)
                self.get_child_nodes().append(child_node)

    def simulation(self):
        cq = CustomQuarto()
        cq.set_players((main.RandomPlayer(cq), main.RandomPlayer(cq)))
        cq.set_starting_player(self.get_active_player())
        cq.set_board(self.get_state().get_board_status())
        flag = True
        if self.get_action_type()[0] == PLACE_PIECE:
            cq.set_selected_piece_index(self.get_action_type()[1])
            flag = False    # skip choose phase (False -> skip it)
        return cq.dynamic_run(flag)

    def backpropagation(self, score):
        self.increase_total_simulations()
        self.add_victory(score)
        current = self
        while current.get_parent_node() is not None:
            current = current.get_parent_node()
            current.increase_total_simulations()
            current.add_victory(score)
        # main_children = [mc for mc in current.get_child_nodes()]
        # for mc in [mc2 for mc2 in main_children if mc2.get_total_simulations() > 0]:
        #     mc.compute_upper_confidence_bound1()

    # GETTERS
    def get_depth_level(self):
        return self.depth_level

    def get_total_simulations(self):
        return self.total_simulations

    def get_child_nodes(self):
        return self.child_nodes

    def get_parent_node(self):
        return self.parent_node

    def get_state(self):
        return self.state

    def get_action_type(self):
        return self.action_type

    def get_upper_confidence_bound(self):
        return self.upper_confidence_bound1

    def get_active_player(self):
        return self.active_player

    # UPDATERS
    def increase_total_simulations(self):
        self.total_simulations += 1

    def add_victory(self, victory_flag):
        self.cumulative_results += victory_flag

    # UTILITIES

    def compute_upper_confidence_bound1(self) -> None:
        if self.get_parent_node() is not None:
            self.upper_confidence_bound1 = \
                (self.cumulative_results / self.total_simulations) + \
                (2 ** 0.5) * ((log(self.parent_node.total_simulations) / self.total_simulations) ** 0.5)
        else:
            self.upper_confidence_bound1 = self.cumulative_results / self.total_simulations

    def choose_best_action(self) -> object:
        best_node = max(self.get_child_nodes(),
                        key=lambda a: a.get_upper_confidence_bound())
        best_action = best_node.get_action_type()[1]
        if isinstance(best_action, tuple):
            best_action = (best_action[1], best_action[0])
        return best_action

    def check_stopping_condition(self):
        if self.get_depth_level() > MAX_DEPTH:
            return True
        for c in self.get_child_nodes():
            if c.check_stopping_condition() is True:
                return True
        return False


class CustomQuarto(quarto.Quarto):
    def __init__(self):
        super().__init__()

    def set_board(self, new_board):
        self._board = new_board

    def set_tile(self, x, y):
        self._board[y, x] = self.get_selected_piece()
        self._binary_board[y, x] = self._Quarto__pieces[self._board[y, x]].binary

    def set_selected_piece_index(self, index):
        self._Quarto__selected_piece_index = index

    def set_starting_player(self, starting_player):
        self._current_player = starting_player

    def set_starting_phase(self, starting_phase):
        self.starting_phase = starting_phase

    def dynamic_run(self, flag) -> float:
        winner = -1
        while winner < 0 and not self.check_finished():
            # self.print()
            if flag:
                piece_ok = False
                while not piece_ok:
                    piece_ok = self.select(self._Quarto__players[self._current_player].choose_piece())
                self._current_player = (self._current_player + 1) % self.MAX_PLAYERS
            piece_ok = False
            # self.print()
            while not piece_ok:
                x, y = self._Quarto__players[self._current_player].place_piece()
                piece_ok = self.place(x, y)
            if flag is False:
                flag = True
            winner = self.check_winner()
        # self.print()
        if winner == 0:
            return 1
        elif winner == 1:
            return 0
        else:
            return 0.5
